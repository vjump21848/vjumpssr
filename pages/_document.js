import Document, { Head, Main, NextScript, Html } from "next/document";
export default class MyDocument extends Document {
  render() {
    return (
      <Html lang="th">
        <Head>
          <meta charSet="utf-8" />
          <link rel="icon" href="/favicon.ico" />
          <meta
            name="description"
            content="เว็บไซต์รวมผลงานของ นายชาญฤทธิ์ พิศิษฐ์จริง ที่ใช้ในการทำงานจริง และใช้ SSR"
          />
          <meta name="msapplication-TileColor" content="#da532c" />
          <meta name="theme-color" content="#ffffff" />
          <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;400;500&display=swap" rel="stylesheet"></link>
        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}
