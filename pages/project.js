import { Container, Row, Col, Image, Button } from "react-bootstrap";
import Head from "next/head";
import rabbit from "../public/image/rabbitsstore.com.png";
import moandv from "../public/image/moandv.com.png";
import vjump from "../public/image/vjumpkung.vercel.app.png";
import meideas108 from "../public/image/meideas108.com.png";
import Layout from "../components/Layout";
const Mywebsite = () => (
  <div>
    <Head>
      <title>VjumpKunG เว็บไซต์รวมผลงานของ ชาญฤทธิ์ พิศิษฐ์จริง | ผลงาน</title>
    </Head>

    <Container>
      <Row className="justify-content-center">
        <Col md={6}>
          <Image
            width="540"
            height="264"
            src={rabbit}
            fluid
            alt="rabbit"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://rabbitsstore.com"
            >
              rabbitsstore.com
            </Button>
          </div>
        </Col>
        <Col md={6}>
          <Image
            width="540"
            height="264"
            src={moandv}
            fluid
            alt="moandv"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://moandv.com"
            >
              moandv.com
            </Button>
          </div>
        </Col>
        <Col md={6}>
          <br />
          <Image
            width="540"
            height="264"
            src={vjump}
            fluid
            alt="vjump"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://vjumpkung.vercel.app"
            >
              vjumpkung.vercel.app
            </Button>
          </div>
        </Col>
        <Col md={6}>
          <br />
          <Image
            width="540"
            height="264"
            src={meideas108}
            fluid
            alt="meideas108"
            style={{ marginTop: 25 }}
          ></Image>
          <div className="center">
            <Button
              variant="secondary"
              size="lg"
              style={{ marginTop: 25 }}
              href="https://meideas108.com"
            >
              www.meideas108.com
            </Button>
          </div>
        </Col>
      </Row>
    </Container>
  </div>
);

const project = () => (
  <Layout>
    <div className="bglight">
      <div id="BrowserView">
        <Container fluid className="bglight" style={{ marginTop: 50 }}>
          <Row>
            <Col>
              <h1 className="font-weight-bold title center">
                ตัวอย่างเว็บไซต์
              </h1>
            </Col>
          </Row>
          <Row>
            <Col style={{ marginTop: 50 }}>
              <Mywebsite />
            </Col>
          </Row>
        </Container>
      </div>
      <div id="MobileView">
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-mobile"
        >
          <Row>
            <Col>
              <h3 className="font-weight-bold center ipadtitle">
                ตัวอย่างเว็บไซต์
              </h3>
            </Col>
          </Row>
          <Row>
            <Col style={{ marginTop: 50 }}>
              <Mywebsite />
            </Col>
          </Row>
        </Container>
      </div>
      <div id="TabletView">
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-tablet"
        >
          <Row>
            <Col>
              <h1 className="font-weight-bold center ipadtitle">
                ตัวอย่างเว็บไซต์
              </h1>
            </Col>
          </Row>
          <Row>
            <Col style={{ marginTop: 50 }}>
              <Mywebsite />
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  </Layout>
);

export default project;
