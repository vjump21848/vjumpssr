import dynamic from "next/dynamic";
import Head from "next/head";
import Layout from "../components/Layout";
const Welcome = dynamic(() => import("../components/welcome"));
const Paragraph = dynamic(() => import("../components/paragraph"));
const home = () => (
  <Layout>
    <Head>
      <title>
        VjumpKunG เว็บไซต์รวมผลงานของ ชาญฤทธิ์ พิศิษฐ์จริง | ผลงาน
      </title>
    </Head>
    <Welcome />
    <Paragraph />
  </Layout>
);

export default home;
