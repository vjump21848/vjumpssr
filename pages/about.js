import { Container, Row, Col, Image } from "react-bootstrap";
import Head from "next/head";
import github from "../public/image/github.svg";
import githublight from "../public/image/githublight.svg";
import gitlab from "../public/image/gitlab.svg";
import facebook from "../public/image/facebook.svg";
import reactlogo from "../public/image/vjump.jpg";
import Layout from "../components/Layout";

const Githubdarkorlight = () => {
  return (
    <div className="bglight">
      {/*PC View*/}
      <div id="BrowserView">
        <Container fluid className="bglight" style={{ marginTop: 50 }}>
          <Row>
            <Col className="center">
              <h1 className="font-weight-bold title center">About</h1>
              <h1 className="center">Chanrich Pisitjing</h1>
              <h2 className="center">ชาญฤทธิ์ พิศิษฐ์จริง</h2>
              <div>
                <h2 className="about">
                  <picture>
                    <source
                      srcSet={githublight}
                      media="(prefers-color-scheme: dark)"
                    />
                    <Image src={github} height="60" alt="github"></Image>
                  </picture>{" "}
                  <a
                    href="https://github.com/vjumpkung"
                    className="nounderline text-gray"
                    style={{ textDecoration: "none" }}
                  >
                    : vjump21848
                  </a>{" "}
                  &nbsp;
                  <Image src={gitlab} height="60" alt="gitlab"></Image>{" "}
                  <a
                    href="https://gitlab.com/vjump21848"
                    className="nounderline text-gray"
                    style={{ textDecoration: "none" }}
                  >
                    : vjumpkung
                  </a>{" "}
                  &nbsp;
                  <Image src={facebook} height="60" alt="facebook"></Image>{" "}
                  <a
                    href="https://www.facebook.com/chanrich.pisitjing/"
                    className="nounderline text-gray"
                    style={{ textDecoration: "none" }}
                  >
                    : ชาญฤทธิ์ พิศิษฐ์จริง
                  </a>{" "}
                  &nbsp;
                </h2>
              </div>
              <Image src={reactlogo} height="450" roundedCircle></Image>
            </Col>
          </Row>
        </Container>
      </div>
      {/*Mobile View*/}
      <div id="MobileView">
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-mobile"
        >
          <Row>
            <Col className="center">
              <h1 className="font-weight-bold center ipadtitle">About</h1>
              <h1 className="center">Chanrich Pisitjing</h1>
              <h2 className="center">ชาญฤทธิ์ พิศิษฐ์จริง</h2>
              <div>
                <h4>
                  <picture>
                    <source
                      srcSet={githublight}
                      media="(prefers-color-scheme: dark)"
                    />
                    <Image src={github} height="40" alt="github"></Image>
                  </picture>{" "}
                  <a
                    href="https://github.com/vjumpkung"
                    style={{ textDecoration: "none" }}
                  >
                    : vjump21848
                  </a>
                  &nbsp;
                </h4>
                <h4>
                  <Image src={gitlab} height="40" alt="gitlab"></Image>{" "}
                  <a
                    href="https://gitlab.com/vjump21848"
                    style={{ textDecoration: "none" }}
                  >
                    : vjumpkung
                  </a>
                  &nbsp;
                </h4>
                <h4>
                  <Image src={facebook} height="40" alt="facebook"></Image>{" "}
                  <a
                    href="https://www.facebook.com/chanrich.pisitjing/"
                    style={{ textDecoration: "none" }}
                  >
                    : ชาญฤทธิ์ พิศิษฐ์จริง
                  </a>
                  &nbsp;
                </h4>
              </div>
              <Image src={reactlogo} width="300" roundedCircle></Image>
            </Col>
          </Row>
        </Container>
      </div>
      {/*iPad View*/}
      <div id="TabletView">
        <Container
          fluid
          className="bglight"
          style={{ marginTop: 50 }}
          id="content-tablet"
        >
          <Row>
            <Col className="center">
              <h1 className="font-weight-bold center ipadtitle">About</h1>
              <h1 className="center">Chanrich Pisitjing</h1>
              <h2 className="center">ชาญฤทธิ์ พิศิษฐ์จริง</h2>
              <div>
                <h2>
                  <picture>
                    <source
                      srcSet={githublight}
                      media="(prefers-color-scheme: dark)"
                    />
                    <Image src={github} height="60" alt="github"></Image>
                  </picture>{" "}
                  <a
                    href="https://github.com/vjumpkung"
                    className="nounderline text-gray"
                  >
                    : vjump21848
                  </a>{" "}
                  &nbsp;
                </h2>
                <h2>
                  <Image src={gitlab} height="60" alt="gitlab"></Image>{" "}
                  <a
                    href="https://gitlab.com/vjump21848"
                    className="nounderline text-gray"
                  >
                    : vjumpkung
                  </a>{" "}
                  &nbsp;
                </h2>
                <h2>
                  <Image src={facebook} height="60" alt="facebook"></Image>{" "}
                  <a
                    href="https://www.facebook.com/chanrich.pisitjing/"
                    className="nounderline text-gray"
                  >
                    : ชาญฤทธิ์ พิศิษฐ์จริง
                  </a>{" "}
                  &nbsp;
                </h2>
              </div>
              <Image src={reactlogo} height="300" roundedCircle></Image>
            </Col>
          </Row>
        </Container>
      </div>
    </div>
  );
};
const Study = () => {
  return (
    <Container>
      <Row>
        <Col>
          <h1 className="text-center">ประวัติการศึกษา</h1>
        </Col>
      </Row>
      <Row className="justify-content-center">
        <Col md={6}>
          <ul className="bigger">
            <li>อนุบาล โรงเรียนอนุบาลเปี่ยมปัญญา</li>
            <li>ประถมศึกษา โรงเรียนอัสสัมชัญธนบุรี</li>
            <li>มัธยมศึกษาปีที่ 1 - 4 โรงเรียนอัสสัมชัญธนบุรี</li>
            <li>มัธยมศึกษาปีที่ 5 - 6 โรงเรียนบดินทรเดชา (สิงห์ สิงหเสนี) 2</li>
          </ul>
        </Col>
      </Row>
    </Container>
  );
};
const Aboutrender = () => (
  <Layout>
    <Head>
      <title>
        VjumpKunG App เว็บไซต์นี่ใช้ Server-Side Rendering - เกี่ยวกับ
      </title>
    </Head>
    <Githubdarkorlight />
    <br></br>
    <Study />
  </Layout>
);

export default Aboutrender;
