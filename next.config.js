const withImages = require("next-images");
module.exports = {
  exportPathMap: async function () {
    return {
      "/": { page: "/" },
      "/about": { page: "/about" },
      "/project": { page: "/project" },
    };
  },
};
module.exports = withImages({
  webpack(config, options) {
    return config;
  },
});
