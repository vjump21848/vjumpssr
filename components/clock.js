import React from "react";
const ClockFunction = () => {
  /*eslint-disable-next-line*/ 
  const [time, setTime] = React.useState(Date());
  React.useEffect(() => {
    const delay = setInterval(() => {
      setTime(Date());
    }, 1000);
    return () => {
      clearInterval(delay);
    };
  }, []);

  return (
    <div>
      <div id="BrowserView">
        <h4 className="font-weight-bold center" style={{ marginTop: 10 }}>
          {new Date().toLocaleTimeString('th-TH')}
        </h4>
      </div>
      <div id="MobileView">
        <h4
          className="font-weight-bold center"
          id="content-mobile"
          style={{ marginTop: 10 }}
        >
          {new Date().toLocaleTimeString('th-TH')}
        </h4>
      </div>
      <div id="TabletView">
        <h4
          className="font-weight-bold center"
          id="content-tablet"
          style={{ marginTop: 10 }}
        >
          {new Date().toLocaleTimeString('th-TH')}
        </h4>
      </div>
    </div>
  );
};
export default ClockFunction;
