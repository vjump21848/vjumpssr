import React, { useState, useEffect } from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import { init } from "ityped";
const TypingAnimation = () => {
  const [Typing, setTyping] = useState(0);
  useEffect(() => {
    const typing1 = document.querySelector("#typing1");
    const typing2 = document.querySelector("#typing2");
    const typing3 = document.querySelector("#typing3");
    init(typing1, {
      showCursor: false,
      loop: false,
      strings: ["ชาญฤทธิ์ พิศิษฐ์จริง"],
    });
    init(typing2, {
      showCursor: false,
      loop: false,
      strings: ["ที่สนใจเรื่องเทคโนโลยีที่อยู่ในปัจจุบัน และ อยากจะเป็น วิศวะคอม"],
    });
    init(typing3, {
      showCursor: false,
      loop: false,
      strings: ["เว็บไซต์นี้พัฒนาโดยใช้ NextJS Framework"],
    });
    return () => {
      <>
        <h1 align="center" className="title" id="typing1"></h1>
        <h3 align="center" id="typing2" className="aboutme"></h3>
        <h4 align="center" id="typing3" ></h4>
      </>;
    };
  }, [Typing]);
  return (
    <>
      <h1 align="center" className="title" id="typing1"></h1>
      <h3 align="center" id="typing2" className="aboutme"></h3>
      <h4 align="center" id="typing3" ></h4>
    </>
  );
};
const Welcome = () => (
  <div className="bglight profile">
    <Container fluid>
      <Row>
        <Col md={4}>
          <Image
            rounded
            src="./image/portfolio/myprofile.png"
            id="zoom"
            width="590.98"
            height="835.38"
            fluid
            alt="ชาญฤทธิ์ พิศิษฐ์จริง"
          />
        </Col>
        <Col md={8} className="my-auto mx-auto">
          <TypingAnimation />
        </Col>
      </Row>
    </Container>
  </div>
);
export default Welcome;
