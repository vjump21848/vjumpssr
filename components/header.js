import { Container, Row, Col, Navbar, Image, Nav } from "react-bootstrap";
import vjump from "../public/image/vjumplight.svg";
import vjumpdark from "../public/image/vjumpdark.svg";
import Link from "next/link";
const Header = () => (
  <div className="bglight">
    <Container fluid className="navbarcolor">
      <Row>
        <Col>
          <Container fluid>
            <Row>
              <Col>
                <Navbar collapseOnSelect expand="md">
                  <Navbar.Brand href="/">
                    <picture>
                      <source
                        srcSet={vjumpdark}
                        media="(prefers-color-scheme: dark)"
                      />
                      <Image
                        src={vjump}
                        height="50"
                        alt="logo"
                        className="logoneuro"
                      ></Image>
                    </picture>
                  </Navbar.Brand>
                  <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                  <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto"></Nav>
                    <Nav>
                      <Link
                        href="/"
                        passHref
                        className="nounderline"
                        style={{ textDecoration: "none" }}
                      >
                        <a className="nounderline">
                          <h4 className="navbar">
                            &nbsp;&nbsp;หน้าแรก&nbsp;&nbsp;
                          </h4>
                        </a>
                      </Link>

                      <Link
                        href="/project"
                        className="nounderline"
                        style={{ textDecoration: "none" }}
                      >
                        <a className="nounderline">
                          <h4 className="navbar">
                            &nbsp;&nbsp;ผลงาน&nbsp;&nbsp;
                          </h4>
                        </a>
                      </Link>
                      <Link
                        href="/about"
                        className="nounderline"
                        style={{ textDecoration: "none" }}
                      >
                        <a className="nounderline">
                          <h4 className="navbar">
                            &nbsp;&nbsp;เกี่ยวกับ&nbsp;&nbsp;
                          </h4>
                        </a>
                      </Link>
                    </Nav>
                  </Navbar.Collapse>
                </Navbar>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
    </Container>
  </div>
);
export default Header;
