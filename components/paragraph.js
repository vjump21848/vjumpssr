import { useState, React } from "react";
import { Container, Row, Col, Image, Button, Collapse } from "react-bootstrap";
const Example = () => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Button
        onClick={() => setOpen(!open)}
        aria-controls="example-collapse-text"
        aria-expanded={open}
      >
        click
      </Button>
      <Collapse in={open}>
        <div id="example-collapse-text">
          Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus
          terry richardson ad squid. Nihil anim keffiyeh helvetica, craft beer
          labore wes anderson cred nesciunt sapiente ea proident.
        </div>
      </Collapse>
    </>
  );
};
const Paragraph = () => (
  <div id="paragraph">
    <Container>
      <Row>
        <Col className="mx-auto">
          <h1 className="title display-1" align="center">
            สวัสดีครับ!
          </h1>
        </Col>
      </Row>
      <Row>
        <Col md={3} className="mx-auto center">
          <Image
            src="./image/vjump.jpg"
            roundedCircle
            fluid
            width="255"
            height="246.73"
          ></Image>
        </Col>
      </Row>
      <Row>
        <Col>
          <br></br>
          <p className="text-center">
            ผม นายชาญฤทธิ์ พิศิษฐ์จริง กำลังศึกษาอยู่ระดับชั้นมัธยมศึกษาปีที่ 6{" "}
            <strong>โรงเรียนบดินทรเดชา (สิงห์ สิงหเสนี) ๒ </strong>
            แผนการเรียนวิทย์-คณิต ครับ
          </p>
        </Col>
      </Row>
      <Row>
        <Col>
          <h2 className="text-center title display-1">Talents</h2>
        </Col>
      </Row>
      <Row>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/facebookreact.svg"
            width="90"
            height="90"
            className="mx-auto d-block py-1"
          ></Image>
          <h3 className="text-center text-dark">React Developer</h3>
          <p className="text-dark">
            - มาจากตัวผมที่สนใจจะเขียน SPA
            ที่ทำให้เว็บไซต์ของเรามีความใกล้เคียงกับ Application มากขึ้น
            สิ่งที่ผมชอบคือการจัดระเบียบส่วนประกอบต่างๆ ที่สามารถนำมาใช้ใหม่ได้
            และในอนาคตนั้นผมก็จะทำการศึกษา React Native เพื่อใช้ในการสร้าง
            Application ได้
          </p>
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/wordpress.png"
            width="90"
            height="90"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">Wordpress Developer</h3>
          <p className="text-dark">
            - ในการสร้างเว็บไซต์ที่ซับซ้อนผมเลือกใช้ Wordpress ในการพัฒนาขึ้นมา
            ทำให้ลดระยะเวลาในการพัฒนาได้เยอะมาก
          </p>
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/lemp-logos.gif"
            height="90"
            width="192"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">Server Manager</h3>
          <p className="text-dark">
            - ด้วยความที่อยากได้ Server
            ไว้ใช้เองในบ้านและเพื่อไว้ใช้ทำงานในระดับ production ผมจึงประกอบ
            Server เองไว้สำหรับเผยแพร่เว็บไซต์ขึ้นมา{" "}
          </p>
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/python.png"
            height="90"
            width="90"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">Python</h3>
          <p className="text-dark">
            - เป็นภาษาที่ผมชอบมากที่สุดเพราะว่า อ่าน เขียน และ เข้าใจง่าย
            และผมนำมาใช้ในการแก้โจทย์ Algorithm จาก Project Euler{" "}
          </p>
          <br />
          <br />
          <br />
          <br />
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/js.svg"
            height="90"
            width="90"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">Javascript</h3>
          <p className="text-dark">
            - มาจากผมเริ่มต้นในการเขียนเว็บแล้วอยากให้มีลูกเล่นต่างๆ นาๆ
            ซึ่งในปัจจุบันได้ต่อยอดในการเขียน NodeJS
          </p>
          <br />
          <br />
          <br />
          <br />
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/csharp.png"
            height="90"
            width="90"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">C#</h3>
          <p className="text-dark">
            - ภาษาเขียนโปรแกรมที่ผมได้ลอง
            เขียนโปรแกรมเชิงวัตถุขึ้นมาเป็นครั้งแรก โดยใช้ Visual C#
          </p>
          <br />
          <br />
          <br />
          <p className="text-secondary">*ปัจจุบันไม่ได้ใช้ในการทำ project แล้ว</p>
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/java.png"
            height="90"
            width="49.19"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">Java</h3>
          <p className="text-dark">
            - ผมสนใจภาษา Java เพราะว่าผมชอบ เล่นเกม Minecraft ซึ่งจะใช้ในอนาคต
          </p>
          <br />
          <br />
          <br />
          <br />
          <p className="text-secondary">*ปัจจุบันไม่ได้ใช้ในการทำ project แล้ว</p>
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/c.png"
            height="90"
            height="90"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">C</h3>
          <p className="text-dark">
            - ภาษาเขียนโปรแกรมแรก ที่ผมได้ทำการศึกษา ซึ่งศึกษา
            จนสามารถเขียนโปรแกรม ควยคุมบอร์ด Arduino ได้
          </p>
          <br />
          <br />
          <br />
          <p className="text-secondary">*ปัจจุบันไม่ได้ใช้ในการทำ project แล้ว</p>
        </Col>
        <Col md={4} className="bg-light border py-3">
          <Image
            src="./image/kodu.png"
            height="90"
            height="90"
            className="mx-auto d-block"
          ></Image>
          <h3 className="text-center text-dark">Kodu Game Lab</h3>
          <p className="text-dark">
            - เป็นจุดเริ่มต้นในการสร้างเกม โดยการใช้ Visual Programming Language
            จนสามารถสร้างเกมที่สามารถเล่นได้จริง
          </p>
          <br />
          <br />
          <br />
          <p className="text-secondary">*ปัจจุบันไม่ได้ใช้ในการทำ project แล้ว</p>
        </Col>
      </Row>
    </Container>
  </div>
);
export default Paragraph;
